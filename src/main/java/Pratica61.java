


import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time vader = new Time();
        
        vader.addJogador("Goleiro",new Jogador(1,"Fulano"));
        vader.addJogador("Lateral",new Jogador(4,"Ciclano"));
        vader.addJogador("Atacante",new Jogador(10,"Beltrano"));
        
        Time luke = new Time();
        luke.addJogador("Goleiro",new Jogador(1,"João"));
        luke.addJogador("Lateral",new Jogador(7,"José"));
        luke.addJogador("Atacante",new Jogador(15,"Mário"));
        
        Set <String> millenium = vader.getJogadores().keySet();
        System.out.println("Posicao \t Time Vader \t Time Luke");
        
        for(String falcon: millenium){
            System.out.println(falcon + "\t "+vader.getJogadores().get(falcon) + "\t " + luke.getJogadores().get(falcon));
        }
    }
}
