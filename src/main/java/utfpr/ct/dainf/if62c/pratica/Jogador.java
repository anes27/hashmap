/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Gamer
 */
public class Jogador {
    private final int numero;
    private final String jogador;
    
    public Jogador(int numero, String player){
        this.numero=numero;
        this.jogador = player;
    }
    
    @Override
    public String toString(){
        return numero + "-" + jogador;
    }
}
